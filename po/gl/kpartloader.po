# translation of kpartloader.po to Galician
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# mvillarino <mvillarino@users.sourceforge.net>, 2008.
# Marce Villarino <mvillarino@gmail.com>, 2009.
# Adrián Chaves Fernández (Gallaecio) <adriyetichaves@gmail.com>, 2017.
# SPDX-FileCopyrightText: 2024 Adrián Chaves (Gallaecio)
#
msgid ""
msgstr ""
"Project-Id-Version: kpartloader\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-10-16 00:43+0000\n"
"PO-Revision-Date: 2024-11-09 16:02+0100\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Proxecto Trasno (proxecto@trasno.gal)\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 24.08.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Adrián Chaves Fernández (Gallaecio)"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "adrian@chaves.gal"

#: kpartloader.cpp:33
#, kde-format
msgid "&Quit"
msgstr "&Saír"

#: kpartloader.cpp:38
#, kde-format
msgid "&About KPart..."
msgstr "&Sobre KPart…"

#: kpartloader.cpp:53
#, kde-format
msgid "No part named %1 found: %2"
msgstr "Non se atopou ningunha compoñente chamada %1: %2"

#: kpartloader.cpp:84
#, kde-format
msgid "KPartLoader"
msgstr "KPartLoader"

#: kpartloader.cpp:86
#, kde-format
msgid "This is a test application for KParts."
msgstr "Esta é unha aplicación de probas para KParts."

#: kpartloader.cpp:94
#, kde-format
msgid "Name of the part to load, e.g. dolphinpart"
msgstr "O nome da compoñente a cargar, p.ex. dolphinpart"

#. i18n: ectx: Menu (file)
#: kpartloaderui.rc:4
#, kde-format
msgid "&File"
msgstr "&Ficheiro"

#. i18n: ectx: Menu (help)
#: kpartloaderui.rc:8
#, kde-format
msgid "&Help"
msgstr "A&xuda"
